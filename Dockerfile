FROM node:slim as build-stage
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

FROM node:alpine as run-stage
WORKDIR /app
COPY --from=build-stage /app/dist/index.js /app/dist/
COPY --from=build-stage /app/package*.json ./
RUN npm install --omit=dev
EXPOSE 3000
CMD ["node", "dist/index.js"]